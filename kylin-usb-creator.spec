Name:           kylin-usb-creator
Version:        1.2.0.3
Release:        3
Summary:        Usb boot maker 
License:        GPL-3+
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz
patch1:         kylin-usb-creator-1.2.0.3-Fix-some-controls-that-are-not-compatible-with-the-theme.patch

BuildRequires:  libkysdk-waylandhelper-devel
BuildRequires:  gsettings-qt-devel
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  polkit-qt5-1-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  ukui-interface
BuildRequires:  pkgconf
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qtchooser
BuildRequires:  qt5-qtscript-devel
BuildRequires:  libkysdk-qtwidgets-devel
BuildRequires:  qt5-qttools-devel


%description
USB Boot Maker provides system image making function.        
The operation process is simple and easy.                    
You can choose iso image and usb driver and make boot driver 

%prep
%setup -q
%autosetup -n %{name}-%{version} -p1

%build
rm -rf %{_builddir}/%{name}-%{version}/UIControl/Makefile
%{qmake_qt5} %{_qt5_qmake_flags} CONFIG+=enable-by-default  kylin-usb-creator.pro
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
make INSTALL_ROOT=%{buildroot} install

mkdir -p %{buildroot}/etc/bin/
mkdir -p %{buildroot}/usr/share/doc/kylin-usb-creator/
mkdir -p %{buildroot}/usr/share/man/man1/
cp debian/copyright  %{buildroot}/usr/share/doc/kylin-usb-creator/
gzip -c debian/changelog > %{buildroot}/usr/share/doc/kylin-usb-creator/changelog.gz

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{_bindir}/kylin-usb-creator
%{_datadir}/applications/kylin-usb-creator.desktop
%{_datadir}/doc/kylin-usb-creator/changelog.gz
%{_datadir}/doc/kylin-usb-creator/copyright
%{_datadir}/pixmaps/kylin-usb-creator.png
%{_sysconfdir}/dbus-1/system.d/com.kylinusbcreator.systemdbus.conf
%{_bindir}/kylin-usb-creator-sysdbus
%{_datadir}/dbus-1/system-services/com.kylinusbcreator.systemdbus.service
%{_datadir}/glib-2.0/schemas/org.kylin-usb-creator-data.gschema.xml
%{_datadir}/polkit-1/actions/com.kylinusbcreator.systemdbus.policy
%{_datadir}/kylin-user-guide/data/guide/kylin-usb-creator




%changelog
* Mon Dec 9 2024 huayadong <huayadong@kylinos.cn> - 1.2.0.3-3
- DESC:fix build warnings: patchN is deprecated (2 usages found), use patch N (or %patch -P N)

* Wed Nov 20 2024 huayadong <huayadong@kylinos.cn> - 1.2.0.3-2
- add patch1:kylin-usb-creator-1.2.0.3-Fix-some-controls-that-are-not-compatible-with-the-theme.patch

* Tue Apr 09 2024 peijiankang <peijiankang@kylinos.cn> - 1.2.0.3-1
- update to upstream version 1.2.0.3 from openkylin

* Thu Aug 31 2023 yoo <sunyuechi@iscas.ac.cn> - 1.1.2-2
- fix clang build error

* Mon May 08 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.2-1
- update to upstream version 1.1.2

* Wed Mar 22 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.1-4
- add kylin-user-guide of kylin-usb-creator

* Wed Mar 22 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.1-3
- fix version of kylin-usb-creator

* Tue Feb 07 2023 peijiankang <peijiankang@kylinos.cn> - 1.1.1-2
- add build debuginfo and debugsource

* Wed Mar 16 2022 tanyulong <tanyulong@kylinos.cn> - 1.1.1-1
- update to upstream version 1.1.1

* Fri Dec 10 2021 douyan <douyan@kylinos.cn> - 1.0.0-3
- fix min window logic

* Wed Dec 8 2021 douyan <douyan@kylinos.cn> - 1.0.0-2
- add dbus service
* Tue Dec 15 2020 lvhan <lvhan@kylinos.cn> - 1.0.0-1
- update to upstream version 1.0.0-26kord
